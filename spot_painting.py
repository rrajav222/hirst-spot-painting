import colorgram
import turtle
import random

turtle.colormode(255)
timmy = turtle.Turtle()
timmy.speed(10)

colors = colorgram.extract('hirst_spot.jpg', 30)

#list_of_colors = []
# for each in colors:
#    each_color = each.rgb
#    r = each_color.r
#    g = each_color.g
#    b = each_color.b
#    list_of_colors.append((r, g, b))


def get_random_color():
    return random.choice(colors).rgb

timmy.hideturtle()
timmy.up()
timmy.goto(-225, -225)

x_cordinate = timmy.xcor()

for j in range(10):
    for i in range(10):
        y_cordinate = timmy.ycor()
        timmy.dot(20, get_random_color())
        if i != 9:
            timmy.fd(50)
        else:
            timmy.goto(x_cordinate, y_cordinate+50)

screen = turtle.Screen()
screen.exitonclick()
